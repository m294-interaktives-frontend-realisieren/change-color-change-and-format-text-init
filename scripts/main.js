function changeText() {
  const origText = 'Originaltext';
  const changedText = 'Geänderter Text';
  const elParaText = document.querySelector('#textContainer p');
  let textToChangeTo = origText;
  if (elParaText.textContent === origText) {
    textToChangeTo = changedText;
  }
  elParaText.textContent = textToChangeTo;
}

function changeColor() {
  const classGolden = 'gold';
  const classBlue = 'blue';
  const elTextContainer = document.getElementById('textContainer');
  const classesTextContainer = elTextContainer.classList;
  let classToRemove = classGolden;
  let classToAdd = classBlue;
  if (classesTextContainer.contains(classBlue)) {
    classToRemove = classBlue;
    classToAdd = classGolden;
  }
  classesTextContainer.remove(classToRemove);
  classesTextContainer.add(classToAdd);
}

const elBtnChangeText = document.getElementById('change-text');
elBtnChangeText.addEventListener('click', changeText);

const elBtnChangeColor = document.getElementById('change-color');
elBtnChangeColor.addEventListener('click', changeColor);
